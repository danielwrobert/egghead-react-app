import axios from 'axios';

const getRepos = function( username ) {
	return axios.get( `https://api.github.com/users/${ username }/repos` );
};

const getUserInfo = function( username ) {
	return axios.get( `https://api.github.com/users/${ username }` );
};

export default function getGitHubInfo( username ) {
	return axios.all( [getRepos( username ), getUserInfo( username )] )
		.then( ( arr ) => ( { repos: arr[0].data, bio: arr[1].data } ) );
};
